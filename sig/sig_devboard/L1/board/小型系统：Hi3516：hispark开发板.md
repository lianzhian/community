| 开发板名称（芯片型号）        | 满天星系列Taurus AI Camera开发套件(Hi3516D)                  |
| :---------------------------- | :----------------------------------------------------------- |
| 芯片架构                      | Hi3516                                                       |
| CPU频率                       |                                                              |
| 介绍（字数请控制在200字以内） | 基于海思Hi3516DV300芯片，支持OpenHarmony小型系统，实现图像采集、识别、兼容HDMI和LCD屏显示接口、双向语音、红外夜视等功能，广泛应用于智能摄像、安防监控、车载记录仪等。 |
| 承接厂家（全称）              | 润和                                                         |
| 技术联系人                    | 石磊                                                         |
| 市场联系人                    |                                                              |
| 兼容性测评                    | 通过                                                         |
| 代码是否在社区                | 是                                                           |
| 硬件状态                      | ok                                                           |
| 具体进展                      | 已通过兼容性认证                                             |
| OpenHarmony适配版本           | OpenHarmony 4.1 Release、OpenHarmony 4.0 Release、OpenHarmony 3.2 Release、OpenHarmony 3.1 Release、OpenHarmony 3.0 Release |
| 代码仓地址                    | https://gitee.com/openharmony                                |
| 购买链接                      | https://item.taobao.com/item.htm?spm=a1z10.5-c-s.w4002-21152782427.11.7b8637c1q8L2S9&id=622343426064 |
| 论坛技术支持链接              | https://laval.csdn.net/column/64dae30bd4841f4dce9ac534       |
| 上电启动                      | 支持                                                         |
| 内核                          | 支持                                                         |
| 触摸屏                        | 支持                                                         |
| LCD                           | 支持                                                         |
| HDC功能                       | 支持                                                         |
| Setting                       | 支持                                                         |
| SystemUI                      | 支持                                                         |
| Launcher                      | 支持                                                         |
| 应用程序框架                  | 支持                                                         |
| TDD测试                       | 不支持                                                       |
| XTS测试                       | 支持                                                         |
| 电源管理                      | 支持                                                         |
| Camera                        | 支持                                                         |
| 音频                          | 支持                                                         |
| 视频                          | 支持                                                         |
| 分布式                        | 支持                                                         |
| WIFI                          | 支持                                                         |
| GPU                           | 支持                                                         |
| 蓝牙                          | 支持                                                         |
| JS框架                        | 支持                                                         |
| 南向IDE                       | 支持                                                         |
| 开发板可获取                  | 支持                                                         |
|                               |                                                              |
|                               |                                                              |