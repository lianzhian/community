| 开发板名称（芯片型号）        | 小熊派BearPi-HM Nano（HI3861）                               |
| :---------------------------- | :----------------------------------------------------------- |
| 芯片架构                      |                                                              |
| CPU频率                       |                                                              |
| 介绍（字数请控制在200字以内） | 小熊派BearPi-HM Nano外型上延续了三段式设计，搭载OpenHarmony操作系统；板上搭载的Hi3861RNIV100，是一款高度集成的2.4GHz Wi-Fi SoC芯片Hi3861，板上搭载的NT3H1x01W0FHKH NFC芯片，是一款简单，低成本的NFC标签；板上搭载的E53标准接口，是一种资源丰富，易于扩展的标准接口，便于多应用的开发和部署，可扩展智能加湿器、智能台灯、智能安防、智能烟感等案例；并配套有DevEco Device Tool开发、调试环境，全套教程以及实验案例。 |
| 承接厂家（全称）              | 小熊派                                                       |
| 技术联系人                    | 王城                                                         |
| 市场联系人                    |                                                              |
| 兼容性测评                    | 通过                                                         |
| 代码是否在社区                | 是                                                           |
| 硬件状态                      | ok                                                           |
| 具体进展                      | 已合入主仓                                                   |
| OpenHarmony适配版本           | OpenHarmony 1.1.0 ReleaseOpenHarmony 3.1 Release             |
| 上电启动                      | 支持                                                         |
| 内核                          | 支持                                                         |
| 触摸屏                        | 不支持                                                       |
| LCD                           | 不支持                                                       |
| **HDC功能**                   | 不支持                                                       |
| **Setting**                   | 不支持                                                       |
| SystemUI                      | 不支持                                                       |
| Launcher                      | 不支持                                                       |
| 应用程序框架                  | 不支持                                                       |
| TDD测试                       | 不支持                                                       |
| XTS测试                       | 支持                                                         |
| 电源管理                      | 支持                                                         |
| Camera                        | 不支持                                                       |
| 音频                          | 不支持                                                       |
| 视频                          | 不支持                                                       |
| 分布式                        | 不支持                                                       |
| WIFI                          | 支持                                                         |
| GPU                           | 不支持                                                       |
| 蓝牙                          | 不支持                                                       |
| JS框架                        | 不支持                                                       |
| 南向IDE                       | 支持                                                         |
| 开发板可获取                  | 支持                                                         |
| 代码仓地址                    | [废弃仓：https://gitee.com/openharmony/device_bearpi_bearpi_hm_nano**新仓库地址：**[device_board_bearpi] (https://gitee.com/openharmony/device_board_bearpi)，[device_soc_hisilicon](https://gitee.com/openharmony/device_soc_hisilicon)](https://gitee.com/openharmony/device_bearpi_bearpi_hm_nano) |
| 购买链接                      | https://item.taobao.com/item.htm?id=633296694816             |