# L0-Device

## 介绍

L0级别的开发板介绍

## 海思Hi3861芯片

[小熊派BearPi-HM-Nano（HI3861）](board/轻量系统：小熊派BearPi-HM-Nano（HI3861）.md)

[润和HH-HSP101满天星系列Pegasus智能家居开发套件](board/轻量系统：润和HH-HSP101满天星系列Pegasus智能家居开发套件（HI3861）.md)

## 芯海cst85

[芯海cst85_wblink（CST85F01）](轻量系统：芯海cst85_wblink（CST85F01）.md)

## 联盛德W800

[海王星系列HH-SLNPT100（联盛德W800）](轻量系统：海王星系列HH-SLNPT100（联盛德W800）.md)

## BES2600WM

[恒玄V200Z-R（BES2600WM）](board/轻量系统：恒玄V200Z-R（BES2600WM）.md)

## STM32F407

[开鸿智谷Niobe407（STM32F407IGT6）](board/轻量系统：开鸿智谷Niobe407（STM32F407IGT6）.md)

## ESP32

[开鸿智谷NiobeU4（ESP32-U4WDH）](轻量系统：开鸿智谷NiobeU4（ESP32-U4WDH）.md)

## ASR5822

[朗国IoT WiFi开发板（ASR5822）](轻量系统：朗国IoT WiFi开发板（ASR5822）.md)

## 全志XR806

[全志XR806](轻量系统：全志XR806（XR806AF2L）.md)





## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
