# OpenHarmony南向生态

### 组织介绍

OpenHarmony硬件芯片工作组 仓库，用于对外展示OpenHarmony硬件生态成果

本仓库说明：

（1）本仓库所列举芯片开发板都已开源到SIG/TPC仓，或者厂家有对应的开源仓库

（2）所以芯片开发板都已验证编译通过，如编译出错请向硬件芯片工作组反馈寻求帮助

---

### 标准系统（L2）

#### 瑞芯微平台：

[RK3568芯片](L2/soc/标准系统：RK3568.md)

#### 展锐平台

[P7885](L2/soc/标准系统：P7885.md)

#### RISC-V平台

[曳影1520](L2/soc/标准系统：TH1520.md)

---

### 小型系统（L1）

#### 海思平台：

[Hi3516DV300](L1/soc/小型系统：Hi3516.md)

---

### 轻量系统（L0）

#### 海思平台：

[Hi3861](L0/soc/轻量系统：Hi3861.md)





### 联系

13510979604



### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
