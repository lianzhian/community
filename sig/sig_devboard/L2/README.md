# L2-Device

## 介绍
L2级别的开发板介绍

## 瑞芯微 RK3568 芯片

[DAYU200开发套件](board/标准系统：DAYU200开发套件(RK3568).md)

[KHDVK-3568A智慧屏开发套件](board/标准系统：KHDVK-3568A智慧屏开发套件(RK3568).md)

[优博终端3568工控](board/标准系统：优博终端3568工控（RK3568）.md)

[扬帆系列“竞”开发板RK3568](board/标准系统：扬帆系列“竞”开发板RK3568.md)

[鸿元智通 HM-3568工业级鸿蒙核心板](board/标准系统：鸿元智通HM-3568工业级鸿蒙核心板.md)

[鸿诚志远HCPAD-100 RK3568](board/标准系统：鸿诚志远HCPAD-100-RK3568.md)

## 瑞芯微 RK3566 芯片

[KHDVK-3566B智慧屏开发套件](board/标准系统：KHDVK-3566B智慧屏开发套件.md)

## 瑞芯微 RK3399

[软通扬帆富设备开发板RK3399](board/标准系统：软通扬帆富设备开发板RK3399.md)

## 展锐 8541E

[DAYU110SL8541E](board/标准系统：DAYU110(SL8541E).md)

## NXP i.MX 8M Mini

[OSWare 大牛-8M Mini](board/标准系统：OSWare-大牛-8M-Mini.md)

## Amlogic A311D

[Unionpi TigerA311D](board/标准系统：Unionpi-Tiger-A311D.md)

## Hi3751V351

[上海海思Phoenix 开发板](board/标准系统：上海海思Phoenix开发板Hi3751V351.md)

## 全志T507

[全志T507EVB_OH1](board/标准系统：全志T507EVB_OH1.md)

## 高通 Qualcomm QRB5165

[奥思维MILOS-DK-RB5](board/标准系统：奥思维MILOS-DK-RB5.md)





## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
